<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Candidate;
use App\User;
use App\Status;
use App\Department;
use App\Role;
use App\Interview;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;



class InterviewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()

    {
        $interviews = Interview::all();

        return view('interviews.index', compact('interviews'));
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Gate::authorize('create-interview', Auth::user());

        $candidates = Candidate::all();
        $users = User::all();

        return view('interviews.create', compact('candidates','users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $interview = new Interview();
        $interview -> interview = $request->interview; 
        $interview -> date = $request->date;
        $interview -> candidate_id = $request->candidate_id;
        $interview -> user_id = $request->user_id;
        $interview -> save();   
        return redirect('interviews');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function myInterviews()
    {   
        $user = Auth::user(); #the one that logged in
        $interviews =  $user->interview;
   

        if($interviews->isEmpty()){
            Session::flash('nointerviews', 'THERE IS NO INTERVIEWS TO SHOW FOR YOU, YOU CAN ADD A NEW ONE');
        }
        return view('interviews.index', compact('interviews'));

    
    }

    

}
