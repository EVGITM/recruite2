<?php

namespace App;
use Illuminate\Support\Facades\DB;


use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    public function candidate(){
        return $this->hasMany('App\Candidate');
    }

    public static function next($status_id){
        $nextstages = DB::table('nextstages')->where('from', $status_id)->pluck('to');
        return self::find($nextstages)->all();
    }

    public static function allowed($from, $to){
        $allowed = DB::table('nextstages')->where('from', $from)->where('to', $to)->get(); //return colloection! not array !
        if(!$allowed->isEmpty()){
            return TRUE ;} else {
                return FALSE;
            }
        
    }



}
