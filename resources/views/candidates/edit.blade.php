@extends('layouts.app')
@section('title', 'Edit Candidate')

@section('content')
        <h1>Edit Candidate</h1>
         <form class="form-inline" method="post" action = "{{action('CandidatesController@update', $candidate->id)}}">
         @csrf
         @method('PATCH')
        <div class="form-group">
            <label class="mr-sm-2" for="name">Candidate name:</label>
            <input  class="form-control mb-2 mr-sm-2" type="text" name = "name" value = {{$candidate->name}}>
        </div>
        <div class="form-group">
            <label class="mr-sm-2" for="email">Candidate email:</label>
            <input class="form-control mb-2 mr-sm-2" type="text" name = "email" value = {{$candidate->email}}>
        </div>
        <div>
            <input class="form-control mb-2 mr-sm-2"  type= "submit" name = "submit" value = "Update">
        </div>
        </form>
@endsection