@extends('layouts.app')
@section('title', 'Create Candidate')

@section('content')
            <h1 >Craete Candidate</h1>
            
            <form class="form-inline" method="post" action = "{{action('CandidatesController@store')}}">
            @csrf
            <div class="form-group">
                <label class="mr-sm-2" for="name">Candidate name:</label>
                <input class="form-control mb-2 mr-sm-2" type="text" name = "name" >
            </div>
            <div class="form-group">
                <label class="mr-sm-2" for="email">Candidate email:</label>
                <input class="form-control mb-2 mr-sm-2"type="text" name = "email">
            </div>
            <div>
                <input class="form-control mb-2 mr-sm-2" type="submit"  name = "submit" value = "Create">
            </div>
            </form>
@endsection