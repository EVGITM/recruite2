@extends('layouts.app')
@section('title', 'Create Interview')

@section('content')
            <h1 >Craete Interview</h1>
            
            <form class="" method="post" action = "{{action('InterviewsController@store')}}">
            @csrf
            <div class="form-group">
                <label class="mr-sm-2" for="name">Interview Abstract:</label>
                <input class="form-control mb-2 mr-sm-2" type="text"  size="100" maxlength="190" name = "interview" >
            </div>
            <div class="form-group">
                <label class="mr-sm-2" for="email">Interview Date:</label>
                <input class="form-control mb-2 mr-sm-2"type="date" size="40" name = "date">
            </div>

            <div class="form-group ">
                            <label for="candidate_id" class="mr-sm-2">Candidate</label>
                            <div class=''>
               
                                <select class="form-control" name="candidate_id"   >                                                                         
                                @foreach ($candidates as $candidate)
                                  
                                   <option value="{{ $candidate->id }}"> 
                                        {{ $candidate->name }} 
                                    </option>
                                   @endforeach
                                 </select>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="user_id" class="mr-sm-2">Interviewer (User)</label>
                            <div class=''>
               
                                <select class="form-control" name="user_id"   >                                                                         
                                @foreach ($users as $user)
                                  
                                   <option value="{{ $user->id }}"> 
                                        {{ $user->name }} 
                                    </option>
                                   @endforeach
                                 </select>
                            </div>
                        </div>



            <div>
                <input class="form-control mb-2 mr-sm-2" type="submit"  name = "submit" value = "Create">
            </div>
            </form>
@endsection

