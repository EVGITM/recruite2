@extends('layouts.app')
@section('title', 'Interviews')
@section('content')

@if(Session::has('nointerviews'))
    <div class = "alert alert-danger"> 
        {{Session::get('nointerviews')}} 
    </div>
    @endif      

<div class="card">
  <h3 class="card-header text-center font-weight-bold text-uppercase py-4">Candidates Interviews</h3>
  <div class="card-body">
  <div><a class="btn btn-light" role="button" href="{{url('/interviews/create')}}">Add New Interview</a></div>

    <div id="table" class="table-editable">
      <span class="table-add float-right mb-3 mr-2"></span>


        <table class="table table-bordered table-responsive-md table-striped ">
        @csrf

        <thead class="thead-dark text-center">
            <tr>

            <th> Candidate Name</th>
            <th> Interviewer (User)</th>

              <th>Interview Abstract</th>
              <th>Interview Date</th>


            </tr>
        </thead>

              <!-- the table data -->
                   @foreach($interviews as $interview)
            <tr>

                <td class=" text-center">{{$interview->candidate->name}}</td>
                <td class=" text-center">{{$interview->user->name}}</td>

                <td>{{$interview->interview}}</td>
                <td class=" text-center">{{$interview->date}}</td>


                </tr>
            @endforeach
        </table>
@endsection
